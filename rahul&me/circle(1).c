#include<stdio.h>
int main()
{
int r,area,peri;
const float pi=3.14;
printf("enter radius of circle: \n");
scanf("%d",&r);
area=pi*r*r;
peri=2*pi*r;
printf("Area of circle: %d \n",area);
printf("Perimeter of circle: %d \n",peri);
return 0;
}
/****output
batch1@cdac-Vostro-460:~/ajra$ gcc circle.c
batch1@cdac-Vostro-460:~/ajra$ ./a.out
enter radius of circle: 
5
Area of circle: 78 
Perimeter of circle: 31 
batch1@cdac-Vostro-460:~/ajra$ 

**/
