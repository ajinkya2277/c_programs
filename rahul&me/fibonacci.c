#include<stdio.h>
int recur(int);
int main()
{
int n=0,k,i;
printf("enter a length of fibonacci series: \n");
scanf("%d",&k);

	for(i=0;i<k;i++)
	{
		printf("%d\n",recur(n));
		n++;
	}
return 0;
}

int recur(int n)
{
if(n==0)
{
	return 0;
}
else if(n==1)
{
	return 1;
}
else
{
	return (recur(n-1)+recur(n-2));
}
}

/*
batch1@cdac-Vostro-460:~/ajra$ gcc fibonacci.c
batch1@cdac-Vostro-460:~/ajra$ ./a.out
enter a length of fibonacci series: 
5
0
1
1
2
3
/

